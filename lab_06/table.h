#ifndef TABLE_H
#define TABLE_H

#define COLUMN_COUNT 8

struct table_t
{
    double **table = nullptr;
    unsigned int rowCount;
    unsigned int columnCount = COLUMN_COUNT;
};

#endif // TABLE_H
