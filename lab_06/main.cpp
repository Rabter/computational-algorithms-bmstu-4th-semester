#include <iostream>
#include "math_module.h"
#include "io.h"

int main()
{
    int begin_x = 2, end_x = 11;
    double step = 1;
    table_t table;
    if (create_table(table, begin_x, end_x, step) == OK)
    {
        fill_table(table, step, 2);
        print_table(table);
    }
    return 0;
}
