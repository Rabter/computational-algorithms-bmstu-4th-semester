#ifndef __MATH_MODULE_H__
#define __MATH_MODULE_H__

#include "table.h"
#define OK 0
#define ERR_ALLOCATION -1
#define ERR_CREATION -2

void fill_table(table_t &table_t, double h, unsigned int r);
int create_table(table_t &table_t, int begin, int end, double step);

#endif
