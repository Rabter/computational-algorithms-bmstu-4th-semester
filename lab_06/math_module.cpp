#include <cmath>
#include <cstdlib>
#include "math_module.h"

static double a0 = 1;
static double a1 = 2;
static double a2 = 3;

double f(double x)
{    
    return (a0 * x) / (a1 + a2 * x);
}


double diffFunc(double x)
{
    return (a0 * a1) / pow((a1 + a2 * x), 2);
}

int create_table(table_t &table, int begin, int end, double step)
{
    if (begin >= end || step <= 0) return ERR_CREATION;
    if (table.table == nullptr)
    {
        table.rowCount = (end - begin) / step;
        double **mtr = (double**) malloc(table.rowCount * sizeof(double *));

        for (unsigned int i = 0; i < table.rowCount; i++)
            mtr[i] = (double*) calloc(table.columnCount, sizeof(double));
        table.table = mtr;

        if (table.table == nullptr)
            return ERR_ALLOCATION;
        double currentX = begin;
        for (unsigned int i = 0; i < table.rowCount; i++)
        {
            table.table[i][0] = currentX;
            table.table[i][1] = f(currentX);
            currentX += step;
        }

    }
    return OK;
}



double right_diff(double y, double yNext, double h)
{
    // Правая разность
    return (yNext - y) / h;
}

double mid_diff(double yPrev, double yNext, double h)
{
    return (yNext - yPrev) / (2.0 * h);
}

double runge_diff(double yNext, double yPREV, double yNr, double yPr, double r, double h)
{
        double y1 = (yNext - yPREV) / h;
        double y2 = (yNr - yPr) / (h * r);
        return y1 + (y1 - y2) / (r * r - 1);
}
// С повышенной точностью на границах
double border_accuracy_first(double y0, double y1, double y2, double h)
{
    return (-3 * y0 + 4 * y1 - y2) / (2 * h);
}

double border_accuracy_last(double y0, double y1, double y2, double h)
{
    return (3 * y0 - 4 * y1 + y2) / (2 * h);
}
double func(int *a, double x)
{
    return (a[0] * x) / (a[1] + a[2] * x);
}
double get_h(double x1, double x2)
{
    return fabs(x1 - x2);
}

double ksi(double x)
{
    return 1 / x;
}

double eta(double x)
{
    return 1 / f(x);
}

double ksi_diff(double x)
{
    return -1 / pow(x, 2);
}

double eta_diff(double y)
{
    return -1 / pow(y, 2);
}

double eta_ksi_diff()
{
    return a1 / a0;
}

double get_alignment(double x, double y, double rightX, double rightY)
{
    return (y * y / x * x) * right_diff(diffFunc(rightY), diffFunc(y), get_h(diffFunc(x), diffFunc(rightX)));
}

void fill_table(table_t &table, double h, unsigned int r)
{
    for (unsigned int i = 0; i < table.rowCount; i++)
    {
        // 1
        if (i != table.rowCount - 1)
            table.table[i][2] = right_diff(table.table[i][1], table.table[i + 1][1], get_h(table.table[i + 1][0], table.table[i][0]));

        // 2 С повышенной точностью на границах
        if (i == 0)
            table.table[i][3] = border_accuracy_first(table.table[0][1], table.table[1][1], table.table[2][1], h);
        if (i == table.rowCount - 1)
            table.table[i][3] = border_accuracy_last(table.table[i][1], table.table[i - 1][1], table.table[i - 2][1], h);

        // 3 Центральная разность
        if (i != 0 && i != table.rowCount - 1)
            table.table[i][4] = mid_diff(table.table[i - 1][1], table.table[i + 1][1], h);

        // 4. 2-я формула Рунге
        if (i < table.rowCount - r)
            table.table[i][5] = runge_diff(table.table[i + 1][1], table.table[i][1],
                    table.table[i + r][1], table.table[i][1], r, h);

        // 5
        table.table[i][6] = eta_ksi_diff() * ksi_diff(table.table[i][0]) / eta_diff(table.table[i][1]);
        table.table[i][7] = diffFunc(table.table[i][0]);
    }
}
