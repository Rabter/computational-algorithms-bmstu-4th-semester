#include <cstdio>
#include "io.h"

bool to_print(int i, int j, int n)
{
    if (j == 2)
    {
        if (i == n - 1)
            return false;
    }
    else if (j == 3)
    {
        if (i > 0 && i < n - 1)
            return false;
    }
    else if (j == 4)
    {
        if (i < 1 || i > n - 2)
            return false;
    }
    else if (j == 5)
    {
        if (i > n - 3)
            return false;
    }
    return true;
}

void print_table(table_t table)
{
    printf(" ________________________________________________________________________ \n");
    printf("|   x      y(x)    y'(x)   Extremes    Mid    Runge   Alignmers  Real    |\n");
    printf("|________________________________________________________________________|\n");
    for (unsigned int i = 0; i < table.rowCount; i++)
    {
        for (unsigned int j = 0; j < table.columnCount; j++)
        {
            if (j == 0)
                printf("|");
            if (to_print(i,j,table.rowCount))
                printf("%-8.4f ", table.table[i][j]);
            else
                printf("   -     ");
            if (j == table.columnCount - 1)
                printf("|");
        }
        printf("\n|________________________________________________________________________|\n");
    }
}
