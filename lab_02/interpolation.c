#include "interpolation.h"
#include <stdio.h>
#include <math.h>
#include "errors.h"
#include "io.h"

int search_position(double **table, int n, double x, int *position)
{
    for(int m = 0; m < n; m ++)
    {
        if (fabs(table[0][m] - x) <= EPS || (table[0][m] < x && m + 1 < n && table[0][m + 1] > x)
                || (table[0][m] > x && m + 1 < n && table[0][m + 1] < x))
        {
            *position = m;
            if (n == 2)
                *position += 1;
            return OK;
        }
    }

    if (x > table[0][n-1])
        *position = n - 1;
    else
        *position = 0;

    return ERR_EXTRAPOLATION;
}

double h(double **table, int i)
{
    return table[0][i] - table[0][i - 1];
}

double B(double **table, int i)
{
    return -2 * (h(table, i - 1) + h(table, i));
}

double F(double **table, int i)
{
    return -3 * ((table[1][i] - table[1][i - 1]) / h(table, i) - (table[1][i - 1] - table[1][i - 2]) / h(table, i-1));
}

double a(double **table, int i)
{
    return table[1][i - 1];
}

double d(double **table, double *U, int i)
{
   return (U[i+1] - U[i]) / (3 * h(table, i));
}

double b(double **table, double *U , int i)
{
   return ((table[1][i] - table[1][i-1]) /  h(table, i)) - (h(table, i) * (U[i+1] + 2 * U[i])) / 3;
}

void interpolation(double **table, int n, int position, double x)
{

    {
    // printf("%d %d\n", position, n);
    /*
    for (int i = 0; i < n; i++)
    {
        printf("%d %lf\n", i, table[0][i]);
    }
    puts("");
    */

    /*double h[n];
    for (int i = 1; i < n; i++)
        h[i] = table[0][i] - table[0][i - 1];*/

    /*
    puts("h");
    for (int i = 0; i < n; i ++)
        printf("%d %lf \n", i, h[i]);
    puts("");
    */

    /*double A[n];
    for (int i = 2; i < n; i++)
        A[i] = h(table, i - 1);*/

    /*
    puts("A");
    for (int i = 0; i < n; i ++)
        printf("%d %lf \n", i, A[i]);
    puts("");
    */

    /*double B[n];
    for (int i = 2; i < n; i++)
        B[i] = -2 * (h(table, i - 1) + h(table, i));
        */

    /*
    puts("B");
    for (int i = 0; i < n; i ++)
        printf("%d %lf \n", i, B[i]);
    puts("");
    */

    /*double D[n];
    for (int i = 2; i < n; i++)
        D[i] = h(table, i);*/

    /*
    puts("D");
    for (int i = 0; i < n; i ++)
        printf("%d %lf \n", i, D[i]);
    puts("");
    */
    /*double F[n];
    for (int i = 2; i < n; i ++)
        F[i] = -3 * ((table[1][i] - table[1][i - 1]) / h(table, i) - (table[1][i - 1] - table[1][i - 2]) / h(table, i-1));
    */

    /*
    puts("F");
    for (int i = 0; i < n; i ++)
        printf("%d %lf \n", i, F[i]);
    puts("");
    */



    /*
    puts("psi");
    for (int i = 0; i <= n; i ++)
        printf("%d %lf \n", i, psi[i]);
    puts("");
    */


    /*
    puts("nyu");
    for (int i = 0; i <= n; i ++)
        printf("%d %lf \n", i, nyu[i]);
    puts("");
    */


    /*
    puts("U");
    for (int i = 0; i <= n; i ++)
        printf("%d %lf \n", i, U[i]);
    puts("");
    */

    /*
    double a[n];
    for(int i = 1; i < n; i++)
        a[i] = table[1][i-1];
        */

    /*
    puts("a");
    for (int i = 0; i <= n; i ++)
        printf("%d %lf \n", i, a[i]);
    puts("");
    */

    /*double d[n];
    for(int i = 1; i < n; i++)
        d[i] = (U[i+1] - U[i]) / (3 * h(table, i));
        */

    /*
    puts("d");
    for (int i = 0; i <= n; i ++)
        printf("%d %lf \n", i, d[i]);
    puts("");
    */

    /*
    double b[n];
    for(int i = 1; i < n; i++)
        b[i] = ((table[1][i] - table[1][i-1]) /  h(table, i)) - (h(table, i) * (U[i+1] + 2 * U[i])) / 3;
    */

    /*
    puts("b");
    for (int i = 0; i <= n; i ++)
        printf("%d %lf \n", i, b[i]);
    puts("");
    */
    }

    double psi[n+1];
    psi[3] = h(table, 2) / B(table, 2);
    for(int i = 3; i <= n; i++)
        psi[i+1] = h(table, i) / (B(table, i) - h(table, i) * psi[i]);

    double nyu[n+1];
    nyu[3] = F(table, 2) / B(table, 2);
    for(int i = 3; i <= n; i++)
       nyu[i+1] = (h(table, i - 1) * nyu[i] + F(table, i)) / (B(table, i) - h(table, i - 1) * psi[i]);

    double U[n + 1];
    if (n < 3)
    {
        for (int i = 0; i < n; ++i)
            U[i] = 0;
    }
    else
    {
        U[1] = 0;
        U[n] = 0;
        for (int i = n-1; i >= 2; i--)
            U[i] = psi[i+1] * U[i+1] + nyu[i+1];
    }

    double result = a(table, position) + b(table, U, position) * (x - table[0][position-1]) + U[position] * \
            (x - table[0][position-1]) * (x - table[0][position-1]) + d(table, U, position) * (x - table[0][position-1]) \
            * (x - table[0][position-1]) * (x - table[0][position-1]);
    printf("Result = %lf\n", result);
    printf("Real value = %lf\n", func(x));
}
