#ifndef INTERPOLATION_H
#define INTERPOLATION_H

int search_position(double **table, int n, double x, int *position);
void interpolation(double **table, int n, int position, double x);

#endif // INTERPOLATION_H
