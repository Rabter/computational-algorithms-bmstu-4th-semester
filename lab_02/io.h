#ifndef IO_H
#define IO_H

#include <stdio.h>
#define START 0.0
#define STEP 1

double func(double x);
double **allocate_matrix(int n, int m);
void free_matrix(double **ptrs, int n);
void print_table(double **p, int n);
void print_matrix(double **p, int n, int m);
int read_table(FILE *f, double ***p, int *n);
void fill_table(FILE *f, int n);

#endif // IO_H
