#include "errors.h"
#include "io.h"
#include <stdio.h>
#include <stdlib.h>

double func(double x)
{
    return x*x;
}

double **allocate_matrix(int n, int m)
{
    double **data = calloc(n, sizeof(double *));

    if (!data)
        return NULL;

    for (int i = 0; i < n; i ++)
    {
        data[i] = malloc(m * sizeof (double));
        if (data[i] == NULL)
        {
            free_matrix(data, n);
            return NULL;
        }
    }

    for (int i = 0; i < n; i ++)
        for (int j = 0; j < m; j ++)
            data[i][j] = 0;
    return data;
}

void free_matrix(double **ptrs, int n)
{
    for (int i = 0; i < n; i ++)
        free(ptrs[i]);
    free(ptrs);
}

///*
void print_matrix(double **p, int n, int m)
{
    for (int i = 0; i < n; i ++)
    {
        for (int j = 0; j < m; j ++)
            printf("%.4lf ", p[i][j]);
        puts("");
    }
    puts("");
}
//*/

void print_table(double **p, int n)
{
    printf("\nTable:\n");
    for (int i = 0; i < n; i ++)
    {
        printf("%d %.4lf %.4lf\n", i, p[0][i], p[1][i]);
    }
    puts("");
}

int read_table(FILE *f, double ***p, int *n)
{
    int rc = OK;
    double num1 = 0, num2 = 0;
    if (fscanf(f, "%d", n) == 1)
    {
        printf("%d\n", *n);
        *p = allocate_matrix(2, *n);
        if (*p != NULL)
        {
            for (int i = 0; i < *n; i ++)
            {
                if (fscanf(f, "%lf %lf", &num1, &num2) != 2)
                    free_matrix(*p, 2);
                (*p)[0][i] = num1;
                (*p)[1][i] = num2;
            }
        }
        else
            rc = ERR_MEM;
        fclose(f);
    }
    else
        rc = ERR_IO;
    return rc;
}

void fill_table(FILE *f, int n)
{
    fprintf(f, "%d\n", n);
    for (int i = 0; i < n; i ++)
    {
        fprintf(f, "%.4lf %.4lf\n", START + STEP * i, func(START + STEP * i));
    }
}
