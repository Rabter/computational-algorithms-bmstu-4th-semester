#include <stdio.h>
#include "io.h"
#include "interpolation.h"
#include "errors.h"

int main(void)
{
    setbuf(stdout, NULL);
    int n;
    int rc = OK;
    double **table = NULL;
    double x = 0;
    FILE *f;
    printf("Enter number of elements or 0 to use an existingtable: ");
    if (scanf("%d", &n) != 1 || n < 0)
    {
        puts("IO ERR!");
        return  ERR_IO;
    }
    if (n)
    {
        f = fopen("input1.txt", "w");
        if (f)
        {
            fill_table(f, n);
            fclose(f);
        }
        else
            rc = ERR_FILE;
    }

    if (rc == OK)
    {
        f = fopen("input1.txt", "r");
        if (f)
        {
            rc = read_table(f, &table, &n);
            if (rc == OK)
            {
                print_table(table, n);
                printf("x = ");
                if (scanf("%lf", &x) != 1)
                {
                    free_matrix(table, 2);
                    puts("Input error");
                    return ERR_IO;
                }
                int position = 0;
                rc = search_position(table, n, x, &position);
                interpolation(table, n, position, x);
                if (rc == ERR_EXTRAPOLATION)
                    puts("Got an extrapolation");
                rc = OK;
                free_matrix(table, 2);
            }
            fclose(f);
        }
    }
}
