#ifndef ERRORS_H
#define ERRORS_H

#define OK 0
#define ERR_IO -1
#define ERR_FILE -2
#define ERR_MEM -3
#define ERR_EXTRAPOLATION -4
#define ERR_INTERPOLATION -5
#define EPS 0.001

#endif // ERRORS_H
