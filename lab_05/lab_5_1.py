from os.path import isfile
from sys import exit

pmin = 3
pmax = 25
coef_multiplier = 7243
N = 40
EPS = 1e-10

def read_data(finname):
    if not isfile(finname):
        print("Input file does not exist")
        exit(-1)
    fin = open(finname, "r")
    line = fin.readline()
    line = line.split()
    if len(line) != 5:
        print("Not enough input data")
        exit(-2)
    Pbeg, Tbeg, T0, Tw, m = map(float, line)
    m = int(m)
    fin.close()
    return Pbeg, Tbeg, T0, Tw, m


def print_input(Pbeg, Tbeg, T0, Tw, m):
    print("Entered data:")
    print("Pbeg =", Pbeg)
    print("Tbeg =", Tbeg)
    print("T0 =", T0)
    print("Tw =", Tw)
    print("m =", m)


def get_nt_array(T0, Tw, p, m):
    nt = []
    z = 0
    i = 0
    coef = coef_multiplier * p
    while i <= N:
        t = T0 + (Tw - T0) * pow(z, m)
        nt.append(coef / t)
        z += 1 / N
        i += 1
    return nt


def integral(T0, Tw, p, m):
    z = step = 1 / N
    nt = get_nt_array(T0, Tw, p, m)
    result = 0
    temp_res = step * nt[0] / 2
    for i in range(1, N):
        result += nt[i] * z
        z += step
    result *= step
    result += temp_res
    return result


def get_p(Pbeg, Tbeg, T0, Tw, m):
    p1 = pmin
    p2 = pmax
    pmid = (p1 + p2) / 2
    coef = coef_multiplier * Pbeg / Tbeg
    fp1 = coef - 2 * integral(T0, Tw, p1, m)
    fp2 = coef - 2 * integral(T0, Tw, p2, m)
    fpmid = coef - 2 * integral(T0, Tw, pmid, m)
    i = 0
    while abs(fpmid) > EPS:
        i += 1
        if fp1 * fpmid < 0:
            p2 = pmid
        elif fp2 * fpmid < 0:
            p1 = pmid
        else:
            print("Can not find P value")
            exit(-3)
        pmid = (p1 + p2) / 2
        fp1 = coef - 2 * integral(T0, Tw, p1, m)
        fp2 = coef - 2 * integral(T0, Tw, p2, m)
        fpmid = coef - 2 * integral(T0, Tw, pmid, m)
    return pmid


if __name__ == "__main__":
    finname = "input.txt"
    Pbeg, Tbeg, T0, Tw, m = read_data(finname)
    print_input(Pbeg, Tbeg, T0, Tw, m)
    print("Calculated P:", get_p(Pbeg, Tbeg, T0, Tw, m))
