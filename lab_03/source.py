import os
import re
import sys


def sum_sqrt(x, y):
    return x * x + y * y


def is_float(x):
    if re.fullmatch(r'[-+]?(?:\d+(?:\.\d*)?|\.\d+)', x) is None:
        return False
    return True


def get_input():
    rc = x = y = power_y = 0
    power_x = input("Введите степень полинома при интерполяции по Ox:\n")
    if not power_x.isdigit():
        rc = 1
    else:
        power_x = int(power_x)
        if not power_x > 0:
            rc = 1
    if rc == 0:
        power_y = input("Введите степень полинома при интерполяции по Oy:\n")
        if not power_y.isdigit():
            rc = 1
        else:
            power_y = int(power_y)
            if not power_y > 0:
                rc = 1
    if rc == 0:
        x = input("Введите абсциссу точки:\n")
        if not is_float(x):
            rc = 2
    if rc == 0:
        y = input("Введите ординату точки:\n")
        if not is_float(y):
            rc = 2
    if rc == 1:
        print("Ошибка: степень должа быть натуральным числом")
    elif rc == 2:
        print("Ошибка: координаты точки дожлы быть действительными числами")
    else:
        x = float(x)
        y = float(y)
        power_x = int(power_x)
        power_y = int(power_y)
    return rc, (x, y), power_x, power_y


#   Line interpolation functions block   #
def create_diftable(x, y, power):
    diftable = list()
    diftable.append(x)
    diftable.append(y)
    for i in range(2, power + 2):
        divdif = []
        for j in range(power - i + 2):
            current = (diftable[i - 1][j] - diftable[i - 1][j + 1]) / (diftable[0][0] - diftable[0][i - 1])
            divdif.append(current)
        diftable.append(divdif)
    return diftable


def p(diftable, x):
    mult = 1
    res = 0
    x = float(x)
    tablen = len(diftable)
    for i in range(1, tablen):
        res += diftable[i][0] * mult
        mult *= (x - diftable[0][i - 1])
    return res


def linterp(x, y, point_x, power):
    diftable = create_diftable(x, y, power)
    rc = 0
    if point_x > diftable[0][len(diftable[0]) - 1] or point_x < diftable[0][0]:
        rc = 1

    return rc, p(diftable, point_x)


#########################################


def insert_right(x, x1, x2):
    n = len(x) - 1
    i = n
    while i > 0 and x2 < x[1][i]:
        i -= 1
    x[0].pop(n)
    x[1].pop(n)
    x[0].insert(i, x1)
    x[1].insert(i, x2)


def insert_left(x, x1, x2):
    i = 0
    while i < len(x[1]) and x2 > x[1][i]:
        i += 1
    x[0].pop(0)
    x[1].pop(0)
    x[0].insert(i, x1)
    x[1].insert(i, x2)


def create_table(fin, point, width, height):
    cur = fin.readline()
    if cur == "":
        print("Недостаточно входных данных\n")
        sys.exit(1)

    cur = cur.split()

    if len(cur) < width:
        print("Недостаточно входных данных\n")
        sys.exit(1)

    y = [[], []]
    j = 0
    while j < width:
        i = 0
        cur[j] = float(cur[j])
        while i < len(y[0]) and cur[j] > y[1][i]:
            i += 1
        y[0].insert(i, j)
        y[1].insert(i, cur[j])
        j += 1
    while j < len(cur):
        ld = abs(y[1][0] - point[0])
        rd = abs(y[1][len(y[1]) - 1] - point[0])
        cur[j] = float(cur[j])
        cd = abs(cur[j] - point[0])
        if cd < ld or cd < rd:
            if ld > rd:
                insert_left(y, j, cur[j])
            else:
                insert_right(y, j, cur[j])
        j += 1
    matrix = list()
    while cur != "":
        cur = fin.readline()
        if cur != "":
            cur = cur.split()
            for i in range(len(cur)):
                cur[i] = float(cur[i])
            matrix.append(cur)
    if len(matrix) < height:
        print("Недостаточно входных данных\n")
        sys.exit(1)

    x = [[], []]
    j = 0
    while j < height:
        i = 0
        while i < len(x[0]) and matrix[j][0] > x[1][i]:
            i += 1
        x[0].insert(i, j)
        x[1].insert(i, matrix[j][0])
        matrix[j].pop(0)
        j += 1
    while j < len(matrix):
        ld = abs(x[1][0] - point[1])
        rd = abs(x[1][len(x[1]) - 1] - point[1])
        cd = abs(matrix[j][0] - point[1])
        if cd < ld or cd < rd:
            if ld > rd:
                insert_left(x, j, matrix[j][0])
            else:
                insert_right(x, j, matrix[j][0])
        matrix[j].pop(0)
        j += 1

    z = list()
    for i in x[0]:
        zcur = list()
        for j in y[0]:
            zcur.append(matrix[i][j])
        z.append(zcur)
    return x[1], y[1], z


def double_interpolation(x, y, z, point, power_x, power_y):
    middle_res = list()
    extra_count = 0  # Extrapolation counter
    if power_y > power_x:
        for value in z:
            rc, current_res = linterp(y, value, point[0], power_y)
            extra_count += rc
            middle_res.append(current_res)
        rc, current_res = linterp(x, middle_res, point[1], power_x)
        extra_count += rc
        return extra_count, current_res
    else:
        for i in range(len(z[0])):
            src = list()
            for line in z:
                src.append(line[i])
            rc, current_res = linterp(x, src, point[1], power_x)
            extra_count += rc
            middle_res.append(current_res)
        rc, current_res = linterp(y, middle_res, point[0], power_y)
        extra_count += rc
        return extra_count, current_res


def print_result(result, realprint=False, point=(0, 0)):
    print("\nКоличество экстраполяций:", result[0])
    print("\nРассчитанный результат:\nz(x, y) = %.7f" % result[1])
    if realprint:
        print("Реальный результат:\nz(x, y) = %.7f" % function(point[0], point[1]))


def print_table(x, y, z):
    print(' ' * 6, ' ' * 2, sep='y\\x', end='')
    for i in x:
        print("%11.5f" % i, sep='', end='')
    print(end='\n')
    for i in range(len(z)):
        print("%11.5f" % y[i], sep='', end='')
        for j in range(len(z[i])):
            print("%11.5f" % z[i][j], sep='', end='')
        print(end='\n')


finname = "input.txt"
realprint = True
function = sum_sqrt

rc, point, power_x, power_y = get_input()
if rc:
    sys.exit(1)
print("Введенны данные:\n", "\nСтепень многочлена по Ox = ", power_x, "\nСтепень многочлена по Oy = ", power_y,
      "\nАбцисса точки = ", point[0], "\nОрдината точки = ", point[1], sep='')
if not os.path.isfile(finname):
    print("Отсутствует файл с исходными данными")
    sys.exit(2)
fin = open(finname, "r")
x, y, z = create_table(fin, point, power_y + 1, power_x + 1)
print_table(x, y, z)
print_result(double_interpolation(x, y, z, point, power_x, power_y), realprint, point)
