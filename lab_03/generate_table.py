def sum_sqrt(x, y):
    return x * x + y * y


foutname = "input.txt"
function = sum_sqrt
get_x = lambda: [i for i in range(0, 10, 1)]
get_y = lambda: [i for i in range(0, 10, 1)]

x = get_x()
y = get_y()
fout = open(foutname, "w")
fout.write(' ')
for i in x:
    fout.write(' ' + str(i))
for j in y:
    fout.write('\n')
    fout.write(str(j))
    for i in x:
        fout.write(' ' + str(function(i, j)))
