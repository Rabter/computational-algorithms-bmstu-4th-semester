import math
import os.path
import re
import sys

realprint = False


def cos90x(x):
    return math.cos(math.pi * x / 2)


def is_float(x):
    if re.fullmatch(r'[-+]?(?:\d+(?:\.\d*)?|\.\d+)', x) is None:
        return False
    return True


def get_input():
    power = input("Введите степень полинома:\n")
    if power.isdigit():
        power = int(power)
        if power > 0:
            x = input("Введите точку:\n")
            if is_float(x):
                return 1, power, x
            else:
                print("Ошибка: точка дожла быть действительным числом")
        else:
            print("Ошибка: степень должа быть натуральным числом")
    else:
        print("Ошибка: степень должа быть натуральным числом")
    return 0, 0, 0


def p(diftable, x):
    mult = 1
    res = 0
    x = float(x)
    tablen = len(diftable)
    for i in range(1, tablen):
        res += diftable[i][0] * mult
        mult *= (x - diftable[0][i - 1])
    return res


def insert_point_left(x, y, new):
    xc = new[0]
    yc = new[1]
    i = 0
    while i < len(x) and xc > x[i]:
        i += 1
    x.pop(0)
    y.pop(0)
    x.insert(i, xc)
    y.insert(i, yc)
    return x[0]


def insert_point_right(x, y, new):
    xc = new[0]
    yc = new[1]
    n = len(x) - 1
    i = n
    while i > 0 and xc < x[i]:
        i -= 1
    x.pop(n)
    y.pop(n)
    x.insert(i, xc)
    y.insert(i, yc)
    return x[len(x) - 1]


def create_list(x, y, power, fin, point_x):
    point_x = float(point_x)
    while len(x) < power + 1:
        cur = fin.readline()
        if cur == "":
            print("Недостаточно данных в файле")
            sys.exit(1)
        cur = cur.split()
        if not (is_float(cur[0]) and is_float(cur[1])):
            print("Ошибка в формате исходных данных в файле")
            sys.exit(1)
        xc, yc = map(float, cur)
        i = 0
        while i < len(x) and xc > x[i]:
            i += 1
        x.insert(i, xc)
        y.insert(i, yc)
    cur = fin.readline()
    while cur != "":
        ld = abs(x[0] - point_x)
        rd = abs(x[power] - point_x)
        cur = cur.split()
        if not (is_float(cur[0]) and is_float(cur[1])):
            print("Ошибка в формате исходных данных в файле")
            sys.exit(1)
        cur[0], cur[1] = map(float, cur)
        cd = abs(cur[0] - point_x)
        if cd < ld or cd < rd:
            if ld > rd:
                ld = insert_point_left(x, y, cur)
            else:
                rd = insert_point_right(x, y, cur)
        cur = fin.readline()


def create_diftable(x, y):
    diftable = list()
    diftable.append(x)
    diftable.append(y)
    for i in range(2, power + 2):
        divdif = []
        for j in range(power - i + 2):
            current = (diftable[i - 1][j] - diftable[i - 1][j + 1]) / (diftable[0][0] - diftable[0][i - 1])
            divdif.append(current)
        diftable.append(divdif)
    return diftable


def print_result(point_x, diftable):
    if point_x > diftable[0][len(diftable[0]) - 1] or point_x < diftable[0][0]:
        print("\nТочка запределами исходных данных, произошла экстраполяция.\n")

    print("\nРассчитанный результат:\ny(x) = %.7f" % p(diftable, point_x))
    if realprint:
        print("Реальный результат:\ny(x) = %.7f" % f(point_x))


finname = "rootfinder.txt"
f = cos90x

rc, power, point_x = get_input()
if rc != 1:
    sys.exit(1)
print("Введенны данные:\n", "Степень многочлена = ", power, "\nАбцисса точки = ", point_x, sep='')
if not os.path.isfile(finname):
    print("Отсутствует файл с исходными данными")
    sys.exit(2)
fin = open(finname, "r")

point_x = float(point_x)

count = 0
x = list()
y = list()

create_list(x, y, power, fin, point_x)
print_result(point_x, create_diftable(x, y))

fin.close()
